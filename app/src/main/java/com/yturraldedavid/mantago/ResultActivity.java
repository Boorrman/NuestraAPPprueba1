package com.yturraldedavid.mantago;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

public class ResultActivity extends AppCompatActivity {

    private ListView listViewLugares;
    public ResultListAdapter m_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_result);
        setContentView(R.layout.activity_result_row);
        listViewLugares = (ListView) findViewById(R.id.listViewLugares);
        //listViewLugares.setOnItemClickListener(this);

        ImageView thumbnail = (ImageView) findViewById(R.id.thumnail);
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                startActivity(intent);
            }
        });
    }

   /* @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, DetailActivity.class);
        startActivity(intent);

    }*/
}
